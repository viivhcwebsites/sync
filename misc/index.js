const fs = require('fs');
const BITBUCKET_USERNAME = process.env.BITBUCKET_USERNAME;
const BITBUCKET_APP_PASSWORD = process.env.BITBUCKET_APP_PASSWORD;

if (!BITBUCKET_USERNAME || !BITBUCKET_APP_PASSWORD) {
    console.error("Please setup BITBUCKET_USERNAME & BITBUCKET_APP_PASSWORD variables first in repository settings")
    process.exit();
}

const releaseDate = new Date().toGMTString().replace("GMT", "+0000");
const filename = process.env.SKETCH_FILE ? process.env.SKETCH_FILE : "sketchlibrary.sketch";
const VERSION = process.env.BITBUCKET_BUILD_NUMBER;
const BITBUCKET_REPO_SLUG = process.env.BITBUCKET_REPO_SLUG;
const BITBUCKET_REPO_OWNER = process.env.BITBUCKET_REPO_OWNER;
const BITBUCKET_GIT_HTTP_ORIGIN = process.env.BITBUCKET_GIT_HTTP_ORIGIN;
const url = BITBUCKET_GIT_HTTP_ORIGIN + "/downloads/" + filename;

let rss = `<?xml version="1.0" encoding="UTF-8"?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:sparkle="http://www.andymatuschak.org/xml-namespaces/sparkle">
    <channel>
        <title>Sketch Library</title>
        <description>Sketch Library</description>
        <image>
            <url></url>
            <title>Sketch Library</title>
        </image>
        <generator>Sketch</generator>
        <item>
            <title>Sketch Library</title>
            <pubDate>${releaseDate}</pubDate>
            <enclosure url="${url}" type="application/octet-stream" sparkle:version="${VERSION}"/>
        </item>
    </channel>
</rss>`


// console.log(BITBUCKET_REPO_SLUG)
// console.log(BITBUCKET_REPO_OWNER)
// console.log(filename)
// console.log(BITBUCKET_APP_PASSWORD)
// console.log(BITBUCKET_USERNAME)
// console.log(url)
// console.log(url)
// console.log(releaseDate)
// console.log(VERSION)
// console.log(rss)
// console.log()
// console.log(BITBUCKET_REPO_FULL_NAME)

fs.writeFileSync("rss.xml", rss);

const uploadSketch = `curl -X POST "https://${BITBUCKET_USERNAME}:${BITBUCKET_APP_PASSWORD}@api.bitbucket.org/2.0/repositories/${BITBUCKET_REPO_OWNER}/${BITBUCKET_REPO_SLUG}/downloads" --form files=@"${filename}"`
const uploadRSS = `curl -X POST "https://${BITBUCKET_USERNAME}:${BITBUCKET_APP_PASSWORD}@api.bitbucket.org/2.0/repositories/${BITBUCKET_REPO_OWNER}/${BITBUCKET_REPO_SLUG}/downloads" --form files=@"rss.xml"`


executeCommand("pwd")
executeCommand("ls")
executeCommand(uploadSketch)
executeCommand(uploadRSS)
console.log("done")

function executeCommand(command) {
    console.info("executing : " + command)
    let child = require('child_process').exec(command)
    child.stdout.pipe(process.stdout)
    child.stderr.pipe(process.stderr)
}


